import os
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict as oDict
from matplotlib.widgets import SpanSelector, Button
import lemming.helpers.lyaclass as lc


# =============================================================================
#                          MAIN FITTING CLASS
# =============================================================================

class lymanIntegrator(lc.LyaFitter):

    def __init__(self, specfile, galaxyName, user='user', nmc=1000,
                 width=4000, showIsmLines=False):
        super().__init__(specfile)
        # Set version names
        self.__version__ = 1.0.0

        # Assign user
        self.user = user

        # Set the name
        self.name = galaxyName

        # Load the redshifts of the sample
        df = pd.read_csv('./lemming/data/redshifts.txt',
                         header=None, names=['name', 'zs'], delimiter='\t')
        df.set_index('name', inplace=True)
        # Select redshift
        try:
            z = df.loc[galaxyName].values
            self.redshift = z[0]
        except KeyError:
            raise KeyError('Cannot find a redshift for galaxy: ', galaxyName)
        # De-redshift the spectrum
        self.shift2Restframe(z)

        # Select the LymanAlpha region
        self.selectWindow(width=width)

        # Initialize variables that will be needed later
        self.contInclude = np.zeros_like(self.lya_wl, dtype=bool)
        self.lyaInclude = np.zeros_like(self.lya_wl, dtype=bool)
        self.maskInclude = np.zeros_like(self.lya_wl, dtype=bool)
        self.lyaStore = np.zeros_like(self.lya_wl, dtype=bool)

        # State variables:
        self.fitCont = True
        self.fitLya = False
        self.fitMask = False

        # Set the number of Monte Carlo iterations
        self.nmc = nmc

        # Set whether or not to show ISM lines
        self.showIsmLines = showIsmLines
        if self.showIsmLines:
            self.ISMlines = self.readISMlines()

        # Create the plot
        self.fig, self.ax = plt.subplots(figsize=(9, 4))
        self.createPlot()

    # -------------------------------------------------------------------------
    #                       Graphical methods
    # -------------------------------------------------------------------------
    def createPlot(self):
        # Create necessary axes
        axCont = self.fig.add_axes([0.93, 0.83, 0.068, 0.07])
        axLya = self.fig.add_axes([0.93, 0.73, 0.068, 0.07])
        axMask = self.fig.add_axes([0.93, 0.63, 0.068, 0.07])
        axClear = self.fig.add_axes([0.93, 0.53, 0.068, 0.07])
        axOk = self.fig.add_axes([0.93, 0.43, 0.068, 0.07])

        #                            Add buttons
        # ---------------------------------------------------------------------
        # Toggle continuum
        self.contButton = Button(axCont, 'Cont')
        if self.fitCont:
            self.contButton.hovercolor = '#9bc8e8'
            self.contButton.color = '#5DA5DA'
        else:
            self.contButton.hovercolor = '#e6e6e6'
            self.contButton.color = '#cccccc'
        self.contButton.on_clicked(self._on_cont_button)

        # Toggle Lya
        self.lyaButton = Button(axLya, r'Ly $\alpha$')
        if self.fitLya:
            self.lyaButton.hovercolor = '#9bc8e8'
            self.lyaButton.color = '#5DA5DA'
        else:
            self.lyaButton.hovercolor = '#e6e6e6'
            self.lyaButton.color = '#cccccc'
        self.lyaButton.on_clicked(self._on_lya_button)

        # Toggle mask
        self.maskButton = Button(axMask, r'Mask')
        if self.fitMask:
            self.maskButton.hovercolor = '#9bc8e8'
            self.maskButton.color = '#5DA5DA'
        else:
            self.maskButton.hovercolor = '#e6e6e6'
            self.maskButton.color = '#cccccc'
        self.maskButton.on_clicked(self._on_Mask_button)

        # Clear current state
        self.clearButton = Button(axClear, 'Clear')
        self.clearButton.color = '#F15854'
        self.clearButton.hovercolor = '#f58884'
        self.clearButton.on_clicked(self._on_clear_button)

        # Save the results
        self.okButton = Button(axOk, 'Save')
        self.okButton.color = '#60BD68'
        self.okButton.hovercolor = '#90d095'
        self.okButton.on_clicked(self._on_ok_button)

        self.fig.canvas.draw()

        # Plot the data
        self.ax.step(self.lya_wl, self.lya_flux)
        self.ax.step(self.lya_wl, self.lya_err)
        # Set xlims
        self.ax.set_xlim(np.min(self.lya_wl), np.max(self.lya_wl))
        # Set ylims so that it does not change when fits are added
        ylims = self.ax.get_ylim()
        self.ax.set_ylim(ylims)

        # If showIsmLines is true, plot them
        if self.showIsmLines:
            self.plotISMlines()

        # Set titles and labels:
        self.ax.set_title(self.name)
        self.ax.set_xlabel(r'Wavelength [$\AA$]')
        self.ax.set_ylabel(r'Flux [ergs s$^{-1}$ cm$^{-2}$ $\AA^{-1}$]')

        plt.subplots_adjust(left=0.08, bottom=0.15, right=0.92, top=0.9,
                            wspace=0.05, hspace=0.05)

        self.span = SpanSelector(self.ax, self._onselect,
                                 'horizontal', useblit=True, minspan=0.1)
        self.fitPlot = None

    #              Deal with Milky Way ISM absorption
    # -------------------------------------------------------------------------
    def readISMlines(self):
        """ Read the Milky way absorption lines from data file
        """
        lines = pd.read_csv('./lemming/data/abslines_uv.dat', header=None,
                            names=['name', 'wl'], delim_whitespace=True)
        lines.wl = lines.wl / (1 + self.redshift)
        return lines

    def plotISMlines(self):
        """ Plot any relevant lines in the plot
        """
        plotrange = self.ax.get_xlim()
        cond = np.where((self.ISMlines.wl > plotrange[0]) &
                        (self.ISMlines.wl < plotrange[1]))
        plotLineWl = self.ISMlines.wl.values[cond]
        plotLineNm = self.ISMlines.name.values[cond]

        yrange = self.ax.get_ylim()

        for i, ln in enumerate(plotLineWl):
            self.ax.axvline(ln, color='#4d4d4d')
            self.ax.text(ln + 0.1, yrange[1]-yrange[1]/15, plotLineNm[i],
                         fontsize=8)
            self.ax.text(ln + 0.1, yrange[1]-2*yrange[1]/15,
                         str(np.round(plotLineWl[i]*(1+self.redshift),
                             decimals=2)), fontsize=8)

    #              Define what happens when selecting a region
    # -------------------------------------------------------------------------
    def _onselect(self, vmin, vmax):
        """ Function called when SpanSelector is used
        """
        # Set everything between vmin and vmax to True.
        mask = np.where((self.lya_wl > vmin) &
                        (self.lya_wl < vmax))
        if self.fitCont:
            self.contInclude[mask] = True
            self.ax.axvspan(vmin, vmax, color='#ef5f00', alpha=0.3, zorder=0,
                            picker=True)
            self._fitContAndUpdate()

        elif self.fitLya:
            self.lyaInclude[mask] = True
            self.lyaStore[self.maskInclude] = self.lyaInclude[self.maskInclude]
            self.ax.axvspan(vmin, vmax, color='#60BD68', alpha=0.3, zorder=0,
                            picker=True)
            self._fitLyaAndUpdate()

        elif self.fitMask:
            # Remove the masked regions from lya fitting
            self.lyaStore[mask] = self.lyaInclude[mask]

            self.lyaInclude[mask] = False
            self.maskInclude[mask] = True
            self.ax.axvspan(vmin, vmax, color='#F15854', alpha=0.5, zorder=0,
                            picker=True)
            self._fitLyaAndUpdate()

        # Update the plot
        self.fig.canvas.draw()

    def replotAxvspan(self, boolarray, color, alpha=0.3):
        if np.any(boolarray):
            start = False
            end = False
            for i, boolean in enumerate(boolarray):
                print(i, boolean)
                if not start:
                    if boolean:
                        start = True
                        start_wl = self.lya_wl[i]
                        print(start_wl)
                else:
                    if not boolean:
                        end = True
                        end_wl = self.lya_wl[i]
                        print(end_wl)
                if end:
                    self.ax.axvspan(start_wl, end_wl, color=color, alpha=alpha,
                                    zorder=0)
                    start = False
                    end = False
        else:
            pass

    #                          Callback functions
    # -------------------------------------------------------------------------

    def _on_cont_button(self, event):
        # Set state variable to fit the continuum
        self.fitCont = True
        self.fitLya = False
        self.fitMask = False
        # Update Button colors:
        self.contButton.hovercolor = '#9bc8e8'
        self.contButton.color = '#5DA5DA'
        self.lyaButton.hovercolor = '#e6e6e6'
        self.lyaButton.color = '#cccccc'
        self.maskButton.hovercolor = '#e6e6e6'
        self.maskButton.color = '#cccccc'

    def _on_lya_button(self, event):
        self.fitLya = True
        self.fitCont = False
        self.fitMask = False
        # Update Button colors:
        self.lyaButton.hovercolor = '#9bc8e8'
        self.lyaButton.color = '#5DA5DA'
        self.contButton.hovercolor = '#e6e6e6'
        self.contButton.color = '#cccccc'
        self.maskButton.hovercolor = '#e6e6e6'
        self.maskButton.color = '#cccccc'

    def _on_Mask_button(self, event):
        self.fitLya = False
        self.fitCont = False
        self.fitMask = True
        # Update Button colors:
        self.maskButton.hovercolor = '#9bc8e8'
        self.maskButton.color = '#5DA5DA'
        self.lyaButton.hovercolor = '#e6e6e6'
        self.lyaButton.color = '#cccccc'
        self.contButton.hovercolor = '#e6e6e6'
        self.contButton.color = '#cccccc'

    def _on_clear_button(self, event):
        # Reset Graph
        self.ax.cla()
        self.createPlot()

        # Reset include vector
        if self.fitCont:
            self.contInclude = np.zeros_like(self.lya_wl, dtype=bool)
            # Replot the non-cleared axvspan
            self.replotAxvspan(self.lyaInclude, color='#60BD68')
            self.replotAxvspan(self.maskInclude, color='#F15854', alpha=0.5)

            self.fitPlot = None

        elif self.fitLya:
            self.lyaInclude = np.zeros_like(self.lya_wl, dtype=bool)
            self.lyaStore = np.zeros_like(self.lya_wl, dtype=bool)

            # Replot the non-cleared axvspan
            self.replotAxvspan(self.contInclude, color='#ef5f00')
            self.replotAxvspan(self.maskInclude, color='#F15854', alpha=0.5)
            if self.fitPlot is None:
                self.fitPlot = self.ax.plot(self.lya_wl, self.cont)
            else:
                self.fitPlot[0].set_data(self.lya_wl, self.cont)

        elif self.fitMask:
            # Use maskInclude to reset the lya include to it's previous value
            self.lyaInclude[self.maskInclude] = self.lyaStore[self.maskInclude]

            self.lyaStore = np.zeros_like(self.lya_wl, dtype=bool)
            self.maskInclude = np.zeros_like(self.lya_wl, dtype=bool)

            # Replot the axspans
            self.replotAxvspan(self.contInclude, color='#ef5f00')
            self.replotAxvspan(self.lyaInclude, color='#60BD68')

            # Replot continuum fit
            if self.fitPlot is None:
                self.fitPlot = self.ax.plot(self.lya_wl, self.cont)
            else:
                self.fitPlot[0].set_data(self.lya_wl, self.cont)

    def _on_ok_button(self, event):
        # Save the data to file:
        fn = './lemming/data/' + self.user + '_measurements.dat'

        # check if the file is empty
        try:
            size = os.stat(fn).st_size
            if size == 0:
                empty = True
            else:
                empty = False
        except FileNotFoundError:
            # file does not exist <=> it is empty
            empty = True

        with open(fn, 'a') as f:
            if empty:
                ln = '#User\tGalaxy\tLya flux\tLya err\tLya EW\tLya EW err\t'
                ln += 'Cont @ Lya\tCont err @ Lya\t'
                ln += 'Cont: k\tCont: kerr\tCont: m\tCont: merr (kx+m)\n'
                f.write(ln)

            ln = ('{0}\t{1}\t{2:.6g}\t{3:.6g}\t{4:.6g}\t{5:.6g}\t{6:.6g}'
                  '\t{7:.6g}\t{8:.6g}\t{9:.6g}'
                  '\t{10:.6g}\t{11:.6g}\n').format(self.user,
                                                   self.name,
                                                   self.lyaIntegral,
                                                   self.lyaIntErr,
                                                   self.lyaEW,
                                                   self.lyaEWerr,
                                                   self.contLC,
                                                   self.contLCerr,
                                                   self.k, self.k_err,
                                                   self.m, self.m_err)
            f.write(ln)
        # Make sure the figures directory exists
        if not os.path.exists('./lemming/data/figures/'):
            os.makedirs('./lemming/data/figures/')
        # Save the plot
        filename = './lemming/data/figures/' + \
                   '{}_{}.png'.format(self.user, self.name)
        self.fig.savefig(filename)

        # place a text box in upper left in axes coords
        props = dict(boxstyle='round', facecolor='#60BD68', alpha=0.9)
        textstr = 'Galaxy data saved\nsuccessfully'
        self.ax.text(0.8, 0.95, textstr, transform=self.ax.transAxes,
                     fontsize=10, verticalalignment='top', bbox=props)

    # -------------------------------------------------------------------------
    #                        Science functions
    # -------------------------------------------------------------------------
    def _fitContAndUpdate(self):
        """ Fits a simple linear polynomial to the selected continuum regions
        """
        # Include based on self.include
        fit_Data = self.lya_flux[self.contInclude]
        fit_Data_w = self.lya_wl[self.contInclude]

        coeff, cov = np.polyfit(fit_Data_w, fit_Data, 1, cov=True)
        self.k = coeff[0]
        self.m = coeff[1]
        self.k_err, self.m_err = self.contMonteCarlo()
        poly = np.poly1d(coeff)
        self.cont = poly(self.lya_wl)
        self.contLC, self.contLCerr = self.calculateLyaCont()

        if self.fitPlot is None:
            self.fitPlot = self.ax.plot(self.lya_wl, self.cont)
        else:
            self.fitPlot[0].set_data(self.lya_wl, self.cont)
        plt.draw()

    def _fitLyaAndUpdate(self):
        """ Integrates the lyman alpha line and does a Monte Carlo error
            estimation.
        """
        intFlux = self.lya_flux[self.lyaInclude] - self.cont[self.lyaInclude]
        intWl = self.lya_wl[self.lyaInclude]

        self.lyaIntegral = np.trapz(intFlux, intWl)
        self.lyaIntErr = self.lyaMonteCarlo()

        self.lyaEW, self.lyaEWerr = self.calculateEW()

        # Show the result in the plot
        textstr = ('Lyman alpha flux:\n {0:.3g} ± {1:.3g}\n\n'
                   'EW: {2:.3g} ± {3:.3g}').format(self.lyaIntegral,
                                                   self.lyaIntErr,
                                                   self.lyaEW, self.lyaEWerr)
        # these are matplotlib.patch.Patch properties
        props = dict(boxstyle='round', facecolor='#5DA5DA', alpha=0.9)

        # place a text box in upper left in axes coords
        self.ax.text(0.05, 0.95, textstr, transform=self.ax.transAxes,
                     fontsize=14, verticalalignment='top', bbox=props)

        plt.draw()

    def contMonteCarlo(self):
        ks = []
        ms = []
        for i in range(self.nmc):
            data = self.redrawData()
            # Include based on self.include
            fit_Data = data[self.contInclude]
            fit_Data_w = self.lya_wl[self.contInclude]

            coeff, cov = np.polyfit(fit_Data_w, fit_Data, 1, cov=True)
            ks.append(coeff[0])
            ms.append(coeff[1])
        k_err = np.std(np.array(ks))
        m_err = np.std(np.array(ms))
        return k_err, m_err

    def lyaMonteCarlo(self):
        ints = []
        for i in range(self.nmc):
            data = self.redrawData()
            intF = data[self.lyaInclude] - self.cont[self.lyaInclude]
            intW = self.lya_wl[self.lyaInclude]

            ints.append(np.trapz(intF, intW))
        int_err = np.std(np.array(ints))
        return int_err

    def redrawData(self):
        errors = np.random.normal(loc=0, scale=1,
                                  size=len(self.lya_err)) * self.lya_err
        newdata = self.lya_flux + errors
        return newdata

    def calculateLyaCont(self):
        poly = np.poly1d([self.k, self.m])
        contLC = poly([1215.67])
        contLCerr = np.sqrt(1215.67*self.k_err**2 + self.m_err**2)
        return contLC[0], contLCerr

    def calculateEW(self):
        # Calculate the EW:
        lyaEW = self.lyaIntegral / self.contLC
        lyaEWerr = np.sqrt(((1/self.contLC)*self.lyaIntErr)**2 +
                           ((self.lyaIntegral / self.lyaIntegral**2)
                           * self.contLCerr)**2)
        return lyaEW, lyaEWerr
