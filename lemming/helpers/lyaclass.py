""" Containerscript for classes used in Lya LineFitting
"""
import sys
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from collections import OrderedDict as oDict
from . import spechelpers as sh


# ==============================================================================
#                        Define main Lya fitter class
# ==============================================================================
class LyaFitter(sh.spectrum):

    def __init__(self, specfile):
        super().__init__(specfile, ivar=False)
        # Rebin by a factor 6 since it is COS spectra
        self.wl = sh.intRebin(self.wl, N=6, pandas=True)
        self.flux, self.err = sh.intRebin(self.flux, err=self.err,
                                          N=6, pandas=True)

    # ======= Isolate the fitting window ======================================
    def selectWindow(self, width=2000):
        """ Method that selects a wavelength window around LymanAlpha (km/s)"""
        # Make sure we are in restframe
        if not self.restframe:
            raise ValueError('Spectrum must be in restframe. \
                             Please run shift2Restframe before selectWindow')

        # Set up a spectrum region that is 1000 km/s wide around 1216Å
        ref_wl = 1215.67
        vrange = np.array([-width/2, width/2])
        wlrange = sh.v2wl(vrange, ref_wl)
        # print(wlrange)

        # Select this region from spectrum
        selector = np.where((self.wl >= wlrange[0]) & (self.wl <= wlrange[1]))
        self.lya_wl = self.wl[selector]
        self.lya_flux = self.flux[selector]
        self.lya_err = self.err[selector]

    # ======= Simple plot of fitting window ===================================
    def plotLya(self):
        """ Plot the LymanAlpha line"""
        plt.figure(figsize=(15, 4))
        plt.plot(self.lya_wl, self.lya_flux, label='flux')
        plt.plot(self.lya_wl, self.lya_err, label='error')
        plt.legend()
