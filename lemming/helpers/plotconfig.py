"""Plotconfig.py
Module which contains nice default settings for matplotlib plots

"""
import matplotlib.pyplot as plt
import matplotlib as mpl
from cycler import cycler
# Seaborn common parameters
# .15 = dark_gray
# .8 = light_gray
colorcycle = ['#5DA5DA', '#F15854', '#60BD68', '#F17CB0',
              '#B2912F', '#B276B2', '#4D4D4D', '#FAA43A', '#DECF3F']
mpl.rcParams['axes.prop_cycle'] = cycler(color=colorcycle)
mpl.rcParams['figure.facecolor'] = 'white'
mpl.rcParams['text.color'] = '.15'
mpl.rcParams['axes.labelcolor'] = '.15'
mpl.rcParams['axes.labelsize'] = '14'
mpl.rcParams['legend.frameon'] = True
mpl.rcParams['legend.fancybox'] = True
mpl.rcParams['legend.numpoints'] = '1'
mpl.rcParams['legend.scatterpoints'] = '1'
mpl.rcParams['xtick.direction'] = 'out'
mpl.rcParams['ytick.direction'] = 'out'
mpl.rcParams['axes.axisbelow'] = True
mpl.rcParams['image.cmap'] = 'viridis'
mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ' Arial', ' Liberation Sans', 'DejaVu Sans'
mpl.rcParams['grid.linestyle'] = '-'
mpl.rcParams['lines.solid_capstyle'] = 'round'
mpl.rcParams['lines.linewidth'] = 1.5

# Seaborn whitegrid parameters
mpl.rcParams['axes.grid'] = False
mpl.rcParams['axes.facecolor'] = 'white'
mpl.rcParams['axes.edgecolor'] = '.30'
mpl.rcParams['axes.linewidth'] = 1.5
mpl.rcParams['grid.color'] = '.8'
mpl.rcParams['xtick.color'] = '.30'
mpl.rcParams['ytick.color'] = '.30'
mpl.rcParams['xtick.major.size'] = '4'
mpl.rcParams['ytick.major.size'] = '4'
mpl.rcParams['xtick.minor.size'] = '2'
mpl.rcParams['ytick.minor.size'] = '2'
mpl.rcParams['xtick.major.width'] = 1.5
mpl.rcParams['ytick.major.width'] = 1.5
mpl.rcParams['xtick.minor.width'] = 1.5
mpl.rcParams['ytick.minor.width'] = 1.5
mpl.rcParams['xtick.minor.visible'] = True
mpl.rcParams['ytick.minor.visible'] = True
mpl.rcParams['xtick.labelsize'] = '13'
mpl.rcParams['ytick.labelsize'] = '13'
