# Welcome to LEMMING
## (Lyman-alpha Estimation through Multi-person Manual Input Numerical inteGration)

## Prerequisites

In order to run the code you need to have the following things set up on your system:

 - python 3.6 (might work on older versions but is untested)
 - numpy
 - scipy
 - pandas
 - matplotlib
 - jupyter notebooks

## How to use lemming:

Start by running the included jupyter notebook `lemming.ipynb` with the command `jupyter notebook lemming.ipynb`.

The notebook is rather self explanatory but, if confusion arises, here is a short summary of the steps that need to be done.

**Prep**

1. Set the user to your username/Initials/whatever you prefer to be known as (preferably no spaces)
2. Set the paths to the data correctly
3. Select galaxy to fit

**Measuring**
1. Select 2 continuum regions by pressing the `cont` button and dragging and dropping in the plot
2. Select 1 lyman alpha region by pressing the `ly a` button. This is the region over which the trapezoidal integration is done.
3. If you are displeased with either the lya or continuum regions press the corresponding button, then `clear` and redo the selection
4. When you are pleased with your results press  `save`


In the background the continuum is fit using a simple linear function and the lyman alpha flux is  integrated across the selected region after continuum subtraction.
